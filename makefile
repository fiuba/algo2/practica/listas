all: main

main: main.o Lista.o
	gcc -Wall -std=c99 main.o Lista.o -o main

main.o: main.c
	gcc -Wall -std=c99 -c main.c

Lista.o: Lista.c
	gcc -Wall -std=c99 -c Lista.c

clean:
	rm -rf *.o
